Lava-Worker
===========

A brief description of the role goes here.

Requirements
------------

It can only run in debian 11 for now due to oficial packages
only being available in Debian.

Role Variables
--------------

| Name                  | Type | Value                                     |
|-----------------------|------|-------------------------------------------|
| worker_interface_name | str  | br0                                       |
| worker_log_level      | str  | DEBUG                                     |
| worker_url            | str  | https://staging.validation.linaro.org/    |
| worker_ws_url         | str  | https://staging.validation.linaro.org/ws/ |
| worker_name           | str  |                                           |
| worker_token          | str  |                                           |
| worker_type           | str  | qemu                                      |


Dependencies
------------

No external dependencies.

Example Playbook
----------------

```yaml
- hosts: dispatcher
  vars:
    worker_interface_name: br0
    worker_log_level: DEBUG
    worker_url: https://staging.validation.linaro.org/
    worker_ws_url: https://staging.validation.linaro.org/ws/
    worker_name: fedora
    worker_token: 12345
  roles:
    - worker
```
License
-------

MIT

Author Information
------------------

Leonardo Rossetti - odra (freenode), lrossetti (matrix), lrossett (fas)
